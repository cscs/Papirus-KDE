# Papirus KDE

# Deprecated

This theme was for Plasma 5 and will no longer be updated.

Papirus KDE - This is a port of the [KDE theme Matchama](https://gitlab.com/cscs/matchama-kde), minimalized with the focus of following user configured color schemes and providing the excellent [Papirus icon theme](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme/) to the Plasma 5 desktop.

In this repository you'll find:

- Plasma Styles ( formerly known as Desktop Themes )

## Installation

Drop the folder(s) into `~/.local/share/plasma/desktopthemes/`

<br></br>

### Donate  

Everything is free, but you can donate using these:  

[<img src="https://az743702.vo.msecnd.net/cdn/kofi4.png?v=2" width=160px>](https://ko-fi.com/X8X0VXZU) &nbsp;[<img src="https://gitlab.com/cscs/resources/raw/master/paypalkofi.png" width=160px />](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M2AWM9FUFTD52)
